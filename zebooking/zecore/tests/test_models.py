from django.test import TestCase

from zecore.models import SearchModel

class SearchModelTest(TestCase):

    def setUp(self):
        
        SearchModel.objects.create(key_words="train paris")
        return super().setUp()
    
    def search_keyword_is_string(self):
        assert 1 == 2
        train_paris = SearchModel.objects.get(key_words="train paris")
        self.assertEqual(type(train_paris.key_words),str)