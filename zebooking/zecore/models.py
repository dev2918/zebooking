from uuid import uuid4

from django.db import models
from django.utils.translation import gettext_lazy as _

class SearchModel(models.Model):

    id = models.UUIDField(primary_key=True,editable=False,default=uuid4())
    key_words = models.CharField(_("Search keys"), null=False, blank=False, max_length=150)
