from django.apps import AppConfig


class ZecoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zecore'
